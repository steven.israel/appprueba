//
//  LoginView.swift
//  AppPrueba (iOS)
//
//  Created by Steve Andrade on 9/12/22.
//

import SwiftUI

struct LoginView: View {
    @StateObject var loginViewModel = LoginViewModel()
    @StateObject var singIn = SingInViewModel()
    
    var body: some View {
        GeometryReader { geometry in
            VStack (alignment: .leading){
                
                VStack (alignment: .center) {
                    Image("logo").resizable().scaledToFit().frame(width: 150, height: 150, alignment: .center)
                    
                    Text("Iniciar sesión").font(.title).fontWeight(.bold)
                }.frame(maxWidth: .infinity, alignment: .center)
                    .padding(.bottom, 13)
                
                
                Text("Nombre de usuario").font(.title3)
                VStack {
                    
                    TextField("Nombre de usuario", text: $loginViewModel.nombre).tint(.black)
                        .frame(maxWidth: .infinity, maxHeight: 50, alignment: .center)
                        
                }.padding(.leading,10)
                    .overlay(RoundedRectangle(cornerRadius: 5)
                        .stroke(.black, lineWidth: 1)
                    )
                
                Text("contraseña").font(.title3)
                VStack {
                    
                    SecureField("contraseña", text: $loginViewModel.contra).tint(.black)
                        .frame(maxWidth: .infinity, maxHeight: 50, alignment: .center)
                        
                }.padding(.leading,10)
                    .overlay(RoundedRectangle(cornerRadius: 5)
                        .stroke(.black, lineWidth: 1)
                    )
                
                Button(action: {
                    singIn.isLogin.toggle()
                }, label: {
                    VStack {
                        Text("Ingresar").font(.title2)
                            .foregroundColor(Color.white )
                            .fontWeight(.bold)
                            .frame(height: 60, alignment: .center)
                    }.frame(maxWidth: .infinity, alignment: .center)
                    .padding(.horizontal, 10)
                    .background(Color("blue-dark"))
                    .cornerRadius(10)
                })
                
                NavigationLink(destination: HomeView(), isActive: $singIn.isLogin, label: {
                    EmptyView()
                })
                
                HStack {
                    Button(action: {
                        singIn.singInGoogle()
                    }, label: {
                        Image("gmail").resizable().scaledToFit().frame(width: 50, height: 40, alignment: .leading)
                        Text("Gmail").font(.title3).fontWeight(.bold).foregroundColor(.black)
                    }).frame(maxWidth: geometry.size.width/2, maxHeight: 60, alignment: .center)
                        .overlay(RoundedRectangle(cornerRadius: 5)
                            .stroke(.black, lineWidth: 1)
                        )
                    
                    Button(action: {
                        singIn.singInFacebook()
                    }, label: {
                        Image("facebook").resizable().scaledToFit().frame(width: 40, height: 50, alignment: .leading)
                        Text("Facebook").font(.title3).fontWeight(.bold).foregroundColor(.black)
                    }).frame(maxWidth: geometry.size.width/2, maxHeight: 60, alignment: .center)
                        .overlay(RoundedRectangle(cornerRadius: 5)
                            .stroke(.black, lineWidth: 1)
                        )
                    
                }.frame(maxWidth: geometry.size.width, alignment: .center).padding(.top, 13)
                
            }.padding(13)
                .navigationBarHidden(true)
                .navigationBarBackButtonHidden(true)
        }
        
        
    }
}

struct LoginView_Previews: PreviewProvider {
    static var previews: some View {
        LoginView()
    }
}
