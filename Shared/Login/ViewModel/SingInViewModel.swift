//
//  SingInGoogleViewModel.swift
//  AppPrueba (iOS)
//
//  Created by Steve Andrade on 9/13/22.
//

import Foundation
import Firebase
import GoogleSignIn
import FacebookCore
import FacebookLogin
import CloudKit

class SingInViewModel: ObservableObject {
    
    @Published var isLogin: Bool = false
    let loginManager = LoginManager()
    
    func singInGoogle() {
        guard let clientID = FirebaseApp.app()?.options.clientID else { return }
                
        // Create Google Sign In configuration object.
        let config = GIDConfiguration(clientID: clientID)
        
        GIDSignIn.sharedInstance.signIn(with: config, presenting: ApplicationUtility.rootViewController){
            user, err in
            
            if let error = err {
                print(error.localizedDescription)
                return
            }
            
            guard let authentication = user?.authentication,
                  let idToken = authentication.idToken
            else {
                return
            }
            
            let credential = GoogleAuthProvider.credential(withIDToken: idToken, accessToken: authentication.accessToken)
            
            Auth.auth().signIn(with: credential) { result, error in
                
                if let err = error {
                    print(err.localizedDescription)
                    return
                }

                guard let user  = result?.user else {
                    return
                }

                print("\(user.displayName ?? "")")
                self.isLogin.toggle()
            }
        }
    }
    
    func singInFacebook() {
        loginManager.logIn(permissions: ["email"], from: nil){
            loginManagerLoginResult, error in
            
            if let error  = error {
                print("error: \(error)")
                return
            }
            
            guard let token  = loginManagerLoginResult?.token?.tokenString else {
                return
            }
            
            print("token: \(token)")
            self.isLogin.toggle()
        }
    }

    
}
