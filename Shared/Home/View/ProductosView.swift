//
//  ProductosView.swift
//  AppPrueba (iOS)
//
//  Created by Steve Andrade on 9/12/22.
//

import SwiftUI

struct ProductosView: View {
    @StateObject var viewModel = ProductosViewModel()
    
    var body: some View {
        GeometryReader { geometry in
            VStack (alignment: .leading) {
                ScrollView {
                    Text("Mis productos").font(.title2).fontWeight(.bold)
                    LazyVGrid (columns: viewModel.formaGrid, spacing: 8) {
                        ForEach(viewModel.productosArray, id: \.self) {
                            producto in
                            
                            Button(action: {
                                viewModel.producto = producto
                                viewModel.sheetProducto.toggle()
                                print(producto.id)
                                
                            }, label: {
                                VStack(alignment: .leading) {
                                    Image(producto.img).resizable().scaledToFit()
                                        .frame(maxWidth: .infinity, alignment: .center)
                                        .cornerRadius(5)
                                    
                                    Text("\(producto.nombre)").padding(.bottom, 10)
                                        .tint(.black)
                                }.padding(.horizontal, 5).padding(.bottom, 13)
                                
                            }).sheet(isPresented: $viewModel.sheetProducto, content: {
                                DetalleProductoView(producto: viewModel.producto)
                            })
                        }
                    }
                } // end scroll
                               
            } // end vstack
            
        } // end geometry
    }
}

struct ProductosView_Previews: PreviewProvider {
    static var previews: some View {
        ProductosView()
    }
}
