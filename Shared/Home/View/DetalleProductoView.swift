//
//  DetalleProductoView.swift
//  AppPrueba (iOS)
//
//  Created by Steve Andrade on 9/12/22.
//

import SwiftUI

struct DetalleProductoView: View {
    var producto: ProductoModel
    @State private var showAlerta = false
    @State private var showAlertaConfirmacion = false
    @Environment(\.presentationMode) var presentationMode
    
    var body: some View {
        GeometryReader { geometry in
            VStack (alignment: .leading){
                
                MapaView().frame(maxWidth: .infinity, maxHeight: geometry.size.height/3, alignment: .center).padding(.top,13)

                VStack(alignment: .leading, spacing: 0) {
                    Image(producto.img).resizable()
                        .frame(maxWidth: 100, maxHeight: 100, alignment: .leading)
                        .cornerRadius(10)

                    Text("\(producto.nombre)").font(.title2).fontWeight(.bold)
                    Text("Los campos de flores y una maravillosa vista").font(.title3)
                        .frame(maxWidth: .infinity, alignment: .leading)
                    
                    Button(action: {
                        self.showAlerta = true
                    }, label: {
                        HStack{
                            Image(systemName: "plus").foregroundColor(.white)
                            
                            Text("Comprar").foregroundColor(.white)
                        }.frame(width: 150, height: 50, alignment: .center)
                        
                    })
                    .background(Color("blue-dark"))
                    .cornerRadius(50)
                    .padding(.vertical,10).alert("la compra se realizó correctamente", isPresented: $showAlertaConfirmacion) {
                        Button("OK", role: .cancel) {
                            presentationMode.wrappedValue.dismiss()
                        }
                    }.alert("Important message", isPresented: $showAlerta) {
                        Button("Confirmar") {
                            self.showAlertaConfirmacion = true
                        }
                        Button("Cancelar") {
                            
                        }
                    }
                    .alert("la compra se realizó correctamente", isPresented: $showAlertaConfirmacion) {
                        Button("OK", role: .cancel) {
                            presentationMode.wrappedValue.dismiss()
                        }
                    }
                    
                }.frame(maxWidth: .infinity, alignment: .leading)
                .padding(.horizontal)
                
                
                
            }.frame(maxWidth: .infinity, alignment: .leading)
            
        }
        
    }
}

struct DetalleProductoView_Previews: PreviewProvider {
    static var previews: some View {
        DetalleProductoView(producto: .getMockData(id: 1, nombre: "Campo", img: "campo"))
    }
}
