//
//  MenuView.swift
//  AppPrueba (iOS)
//
//  Created by Steve Andrade on 9/12/22.
//

import SwiftUI

struct MenuView: View {
    @Binding var option: Int
    var body: some View {
        VStack(alignment: .leading) {
            
            HStack {
                Button(action: {
                    option = 0
                }, label: {
                    
                    Image(systemName: "person.crop.circle")
                        .foregroundColor(.white)
                        .imageScale(.large)
                    Text("Perfil")
                        .foregroundColor(.white)
                        .font(.headline)
                    
                })
                
            }.padding(.top, 100)
            
            HStack {
                Button(action: {
                    option = 1
                }, label: {
                    
                    Image(systemName: "archivebox.circle")
                        .foregroundColor(.white)
                        .imageScale(.large)
                    Text("Mis productos")
                        .foregroundColor(.white)
                        .font(.headline)
                    
                })

            }.padding(.top, 30)
            
            HStack {
                Button(action: {
                    option = 2
                }, label: {
                    
                    Image(systemName: "gear")
                        .foregroundColor(.white)
                        .imageScale(.large)
                    Text("Configuraciones")
                        .foregroundColor(.white)
                        .font(.headline)
                    
                })

            }.padding(.top, 30)
            
            HStack {
                Button(action: {
                    option = 3
                }, label: {
                    Image(systemName: "xmark.circle")
                        .foregroundColor(.white)
                        .imageScale(.large)
                    Text("Cerrar sesión")
                        .foregroundColor(.white)
                        .font(.headline)
                })
                
            }.padding(.top, 30)
            
            Spacer()
            
        }.padding()
            .frame(maxWidth: .infinity, alignment: .leading)
            .background(Color("blue-dark"))
            .edgesIgnoringSafeArea(.all)
        
    }
}

struct MenuView_Previews: PreviewProvider {
    static var previews: some View {
        MenuView(option: .constant(0))
    }
}
