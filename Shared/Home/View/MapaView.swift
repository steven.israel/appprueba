//
//  MapaView.swift
//  AppPrueba (iOS)
//
//  Created by Steve Andrade on 9/12/22.
//

import SwiftUI
import MapKit

struct MapaView: View {
    @StateObject var viewModel = DetalleProductoViewModel()

    var body: some View {
        if viewModel.userHasLocation {

            Map(coordinateRegion: $viewModel.userLocation, showsUserLocation: true)

        } else {
            Link("Pulsa para aceptar la autorización de Localización", destination: URL(string: UIApplication.openSettingsURLString)!)
            .padding(32)
        }
    }
}

struct MapaView_Previews: PreviewProvider {
    static var previews: some View {
        MapaView()
    }
}
