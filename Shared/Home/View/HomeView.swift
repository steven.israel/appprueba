//
//  HomeView.swift
//  AppPrueba (iOS)
//
//  Created by Steve Andrade on 9/12/22.
//

import SwiftUI

struct HomeView: View {
    @State private var openMenu: Bool = false
    @State var optionSelect: Int = 1
    @State private var salir: Bool = false
    
    var body: some View {
        GeometryReader { geometry in
            
            ZStack {
                
                VStack {
                    switch optionSelect {
                        case 0:
                            Text("Perfil")
                        case 1:
                            ProductosView()
                        case 2:
                            Text("Configuraciones")
                        default:
                            Text("Cerrar sesión")
                        
                    }
                }.padding(.horizontal)
                .offset(x: self.openMenu ? geometry.size.width : 0)
                    .disabled(self.openMenu ? true : false)
                
                if self.openMenu {
                    MenuView(option: $optionSelect)
                        .frame(width: geometry.size.width)
                        .transition(.move(edge: .leading))
                }
                
            }
            // end zstack
            
            NavigationLink(destination: LoginView(), isActive: $salir, label: {
                EmptyView()
            })
            
        }.navigationBarTitle("", displayMode: .inline)
            .navigationBarBackButtonHidden(true)
            .navigationBarItems(leading: (
                Button(action: {
                    withAnimation {
                        self.openMenu.toggle()
                    }
                }) {
                    Image(systemName: "line.horizontal.3")
                        .imageScale(.large)
                }
            ))
            .onChange(of: optionSelect, perform: { change in
                if change == 3 {
                    self.salir.toggle()
                } else {
                    withAnimation {
                        self.openMenu.toggle()
                    }
                }
            })
        // end geometry
        
    }
    
    init() {
        UITabBar.appearance().backgroundColor = UIColor(Color("blue-dark"))
        UITabBar.appearance().unselectedItemTintColor = UIColor(Color("blue-dark"))
        UITabBar.appearance().isTranslucent = false
        UINavigationBar.appearance().barTintColor = .white
        UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: UIColor.white]
        UINavigationBar.appearance().largeTitleTextAttributes = [.foregroundColor: UIColor.white]
    }
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
