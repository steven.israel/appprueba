//
//  ProductoModel.swift
//  AppPrueba (iOS)
//
//  Created by Steve Andrade on 9/12/22.
//

import Foundation

struct ProductoModel: Codable, Hashable{
    let id: Int
    let nombre: String
    let img: String
    enum CodingKeys: String, CodingKey {
        case id, nombre, img
    }
}

extension ProductoModel {
    static func getMockData(
        id: Int,
        nombre: String,
        img: String
    ) -> ProductoModel {
        .init(
            id: id,
            nombre: nombre,
            img: img
        )
    }
}
