//
//  ProductosViewModel.swift
//  AppPrueba (iOS)
//
//  Created by Steve Andrade on 9/12/22.
//

import Foundation
import SwiftUI

class ProductosViewModel: ObservableObject {
    @Published var productosArray: [ProductoModel] = []
    @Published var formaGrid = [GridItem(.flexible()), GridItem(.flexible())]
    @Published var sheetProducto = false
    @Published var producto: ProductoModel = .getMockData(id: 1, nombre: "campo", img: "campo")
    
    init (){
        for i in 1...20 {
            productosArray.append(ProductoModel.getMockData(id: i, nombre: "producto", img: "campo"))
        }
    }
    
}
